package main.test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import main.Main;
import model.Group;
import model.Player;

import org.junit.Assert;
import org.junit.Test;

public class MainTest {

	@Test
	public void testOneGroupOnePlayer() {
		List<Group> groups = new ArrayList<Group>();
		
		int amtGroups = 1;
		int amtPlayers = 1;
		int relevantPlayers = 5;
		
		for (int i = 0; i < amtGroups; i++) {
			List<Player> players = new ArrayList<Player>();
			for (int k = 0; k < amtPlayers; k++) {
				Player player = new Player("p" + k, new BigDecimal(k + i));
				players.add(player);
			}
			Group group = new Group("g" + i, players);
			groups.add(group);
		}
				
		List<Group> sortedGroups = Main.sortGroupsAndPlayersByElo(groups, relevantPlayers);
		
		Assert.assertEquals(amtGroups, sortedGroups.size());
		
		int g = amtGroups - 1;
		for (Group group : sortedGroups){
			Assert.assertEquals("g" + g, group.getName());
			Assert.assertEquals(calcAvgElo(amtPlayers, relevantPlayers, g), group.getAvgElo());
			int p = amtPlayers - 1;
			for (Player player : group.getPlayers()) {
				Assert.assertEquals("p" + p, player.getName());
				Assert.assertEquals(new BigDecimal(p + g), player.getElo());
				p--;
			}
			g--;
		}	
	}
	
	@Test
	public void testFiveGroupsFivePlayersReversed() {
		List<Group> groups = new ArrayList<Group>();
		
		int amtGroups = 5;
		int amtPlayers = 5;
		int relevantPlayers = 7;
		
		for (int i = 0; i < amtGroups; i++) {
			List<Player> players = new ArrayList<Player>();
			for (int k = 0; k < amtPlayers; k++) {
				Player player = new Player("p" + k, new BigDecimal(k + i));
				players.add(player);
			}
			Group group = new Group("g" + i, players);
			groups.add(group);
		}
				
		List<Group> sortedGroups = Main.sortGroupsAndPlayersByElo(groups, relevantPlayers);
		
		Assert.assertEquals(amtGroups, sortedGroups.size());
		
		int g = amtGroups - 1;
		for (Group group : sortedGroups){
			Assert.assertEquals("g" + g, group.getName());
			Assert.assertEquals(calcAvgElo(amtPlayers, relevantPlayers, g), group.getAvgElo());
			int p = amtPlayers - 1;
			for (Player player : group.getPlayers()) {
				Assert.assertEquals("p" + p, player.getName());
				Assert.assertEquals(new BigDecimal(p + g), player.getElo());
				p--;
			}
			g--;
		}	
	}
	
	@Test
	public void testFiveGroupsFivePlayersSorted() {
		List<Group> groups = new ArrayList<Group>();
		
		int amtGroups = 5;
		int amtPlayers = 5;
		int relevantPlayers = 7;
		
		for (int i = amtGroups - 1; i >= 0; i--) {
			List<Player> players = new ArrayList<Player>();
			for (int k = amtPlayers - 1; k >= 0; k--) {
				Player player = new Player("p" + k, new BigDecimal(k + i));
				players.add(player);
			}
			Group group = new Group("g" + i, players);
			groups.add(group);
		}
				
		List<Group> sortedGroups = Main.sortGroupsAndPlayersByElo(groups, relevantPlayers);
		
		Assert.assertEquals(amtGroups, sortedGroups.size());
		
		int g = amtGroups - 1;
		for (Group group : sortedGroups){
			Assert.assertEquals("g" + g, group.getName());
			Assert.assertEquals(calcAvgElo(amtPlayers, relevantPlayers, g), group.getAvgElo());
			int p = amtPlayers - 1;
			for (Player player : group.getPlayers()) {
				Assert.assertEquals("p" + p, player.getName());
				Assert.assertEquals(new BigDecimal(p + g), player.getElo());
				p--;
			}
			g--;
		}	
	}
	
	@Test
	public void testFiveGroupsFivePlayersLimitedReversed() {
		List<Group> groups = new ArrayList<Group>();
		
		int amtGroups = 5;
		int amtPlayers = 5;
		int relevantPlayers = 4;
		
		for (int i = 0; i < amtGroups; i++) {
			List<Player> players = new ArrayList<Player>();
			for (int k = 0; k < amtPlayers; k++) {
				Player player = new Player("p" + k, new BigDecimal(k + i));
				players.add(player);
			}
			Group group = new Group("g" + i, players);
			groups.add(group);
		}
				
		List<Group> sortedGroups = Main.sortGroupsAndPlayersByElo(groups, relevantPlayers);
		
		Assert.assertEquals(amtGroups, sortedGroups.size());
		
		int g = amtGroups - 1;
		for (Group group : sortedGroups){
			Assert.assertEquals("g" + g, group.getName());
			Assert.assertEquals(calcAvgElo(amtPlayers, relevantPlayers, g), group.getAvgElo());
			int p = amtPlayers - 1;
			for (Player player : group.getPlayers()) {
				Assert.assertEquals("p" + p, player.getName());
				Assert.assertEquals(new BigDecimal(p + g), player.getElo());
				p--;
			}
			g--;
		}	
	}
	
	@Test
	public void testFiveGroupsFivePlayersLimitedSorted() {
		List<Group> groups = new ArrayList<Group>();
		
		int amtGroups = 5;
		int amtPlayers = 5;
		int relevantPlayers = 4;
		
		for (int i = amtGroups - 1; i >= 0; i--) {
			List<Player> players = new ArrayList<Player>();
			for (int k = amtPlayers - 1; k >= 0; k--) {
				Player player = new Player("p" + k, new BigDecimal(k + i));
				players.add(player);
			}
			Group group = new Group("g" + i, players);
			groups.add(group);
		}
				
		List<Group> sortedGroups = Main.sortGroupsAndPlayersByElo(groups, relevantPlayers);
		
		Assert.assertEquals(amtGroups, sortedGroups.size());
		
		int g = amtGroups - 1;
		for (Group group : sortedGroups){
			Assert.assertEquals("g" + g, group.getName());
			Assert.assertEquals(calcAvgElo(amtPlayers, relevantPlayers, g), group.getAvgElo());
			int p = amtPlayers - 1;
			for (Player player : group.getPlayers()) {
				Assert.assertEquals("p" + p, player.getName());
				Assert.assertEquals(new BigDecimal(p + g), player.getElo());
				p--;
			}
			g--;
		}	
	}
	
	private BigDecimal calcAvgElo(int amtPlayers, int relevantPlayers, int offset) {
		int totalElo = 0;
		int addedPlayers = 0;
		while (amtPlayers > 0 && addedPlayers < relevantPlayers) {
			totalElo += amtPlayers - 1 + offset;
			amtPlayers--;
			addedPlayers++;
		}
		
		return new BigDecimal(totalElo).divide(new BigDecimal(addedPlayers), 2, RoundingMode.HALF_UP);
	}
	
}
