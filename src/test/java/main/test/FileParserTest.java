package main.test;

import java.io.FileNotFoundException;
import java.util.List;

import main.FileParser;
import model.Group;
import model.Player;

import org.junit.Assert;
import org.junit.Test;

public class FileParserTest {
	
	private static final String INPUT_FILES_PATH = "./src/test/resources/inputFiles/";
	
	@Test
	public void testEmptyInputFile() throws FileNotFoundException {
		List<Group> groups = FileParser.getGroups(INPUT_FILES_PATH + "emptyFile.txt");
		
		Assert.assertTrue(groups.isEmpty());
	}
	
	@Test
	public void testInputFileWithOneGroup() throws FileNotFoundException {
		List<Group> groups = FileParser.getGroups(INPUT_FILES_PATH + "oneGroup.txt");
		
		Assert.assertEquals(1, groups.size());
		
		Group group = groups.get(0);
		List<Player> players = group.getPlayers();
		
		Assert.assertEquals("group1", group.getName());
		Assert.assertEquals(3, players.size());
		Assert.assertEquals("player1", players.get(0).getName());
		Assert.assertEquals("player2", players.get(1).getName());
		Assert.assertEquals("player3", players.get(2).getName());
	}
	
	@Test
	public void testInputFileWithTwoGroups() throws FileNotFoundException {
		List<Group> groups = FileParser.getGroups(INPUT_FILES_PATH + "twoGroups.txt");
		
		Assert.assertEquals(2, groups.size());
		
		Group group = groups.get(0);
		List<Player> players = group.getPlayers();
		
		Assert.assertEquals("group1", group.getName());
		Assert.assertEquals(3, players.size());
		Assert.assertEquals("player1", players.get(0).getName());
		Assert.assertEquals("player2", players.get(1).getName());
		Assert.assertEquals("player3", players.get(2).getName());
		
		group = groups.get(1);
		players = group.getPlayers();
		
		Assert.assertEquals("group2", group.getName());
		Assert.assertEquals(3, players.size());
		Assert.assertEquals("player4", players.get(0).getName());
		Assert.assertEquals("player5", players.get(1).getName());
		Assert.assertEquals("player6", players.get(2).getName());
	}

}
