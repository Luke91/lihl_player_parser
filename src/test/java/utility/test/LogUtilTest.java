package utility.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import utility.LogUtil;

public class LogUtilTest {
	
	private static final String LOGFILE_PATH = "./src/test/resources/logfiles/";
	
	@Test
	public void testLoggingAndCreateLogfile() throws IOException {
		String testTextOne = "Test text.";
		String testTextTwo = "Test text 2.";
		String testException = "Test exception.";
		
		LogUtil.log(testTextOne, new IllegalArgumentException(testException));
		LogUtil.log(testTextTwo);
		
		String filename = "testLogfile" + getTimestampForFilename() + ".txt";
		
		LogUtil.createLogfile(LOGFILE_PATH, filename);
	
		File file = new File(LOGFILE_PATH + filename);
		
		Assert.assertTrue(file.exists());
		Assert.assertTrue(file.isFile());
		
		String fileContent = readFile(LOGFILE_PATH + filename);
		
		Assert.assertTrue(fileContent.contains(testTextOne));
		Assert.assertTrue(fileContent.contains(testTextTwo));
		Assert.assertTrue(fileContent.contains(testException));
				
		file.delete();
	}

	private String getTimestampForFilename() {
		return new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
	}
	
	private String readFile(String filePath) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}

			return sb.toString();
		} finally {
			br.close();
		}
	}
}
