package utility.test;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import utility.PropertiesUtil;

public class PropertiesUtilTest {
	
	private static final String PROPERTIES_PATH = "./src/test/resources/propertiesFiles/";
	
	@Test
	public void testAllProperties() throws IllegalArgumentException, IOException {
		PropertiesUtil.loadProperties(PROPERTIES_PATH + "all.properties");
		
		Assert.assertEquals("InputFile", PropertiesUtil.getPlayersInputFile());
		Assert.assertEquals(5, PropertiesUtil.getRelevantPlayersPerGroup());
		Assert.assertEquals(',', PropertiesUtil.getDecimalSeparator());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNoPlayersInputFile() throws IllegalArgumentException, IOException {
		PropertiesUtil.loadProperties(PROPERTIES_PATH + "noPlayersInputFile.properties");
	}
	
	public void testNoRelevantPlayersPerGroup() throws IllegalArgumentException, IOException {
		PropertiesUtil.loadProperties(PROPERTIES_PATH + "noRelevantPlayersPerGroup.properties");
		
		Assert.assertEquals(Integer.MAX_VALUE, PropertiesUtil.getRelevantPlayersPerGroup());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testInvalidRelevantPlayersPerGroup() throws IllegalArgumentException, IOException {
		PropertiesUtil.loadProperties(PROPERTIES_PATH + "invalidRelevantPlayersPerGroup.properties");
	}
	
	@Test(expected = IOException.class)
	public void testNonExistentPropertiesFile() throws IllegalArgumentException, IOException {
		PropertiesUtil.loadProperties(PROPERTIES_PATH + "doesNotExist.properties");
	}
	
	@Test
	public void testNoDecimalSeparator() throws IllegalArgumentException, IOException {
		PropertiesUtil.loadProperties(PROPERTIES_PATH + "noDecimalSeparator.properties");
		
		Assert.assertEquals('.', PropertiesUtil.getDecimalSeparator());
	}

}
