package utility;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Properties;

import utility.LogUtil;

public class PropertiesUtil {
		
	private static final String PLAYERS_INPUT_FILENAME_PROPERTY = "playersInputFile";
	private static final String RELEVANT_PLAYERS_PER_GROUP_PROPERTY = "relevantPlayersPerGroup";
	private static final String DECIMAL_SEPARATOR_PROPERTY = "decimalSeparator";
	
	private static String playersInputFile;
	private static int relevantPlayersPerGroup;
	private static char decimalSeparator;

	public static void loadProperties(String filename) throws IOException, IllegalArgumentException {
		LogUtil.log("Loading properties...");

		Properties properties = new Properties();
		BufferedInputStream inStream = new BufferedInputStream(new FileInputStream(filename));
		properties.load(inStream);
		inStream.close();
		String playersInputFileProperty = properties.getProperty(PLAYERS_INPUT_FILENAME_PROPERTY);
		String relevantPlayersPerGroupProperty = properties.getProperty(RELEVANT_PLAYERS_PER_GROUP_PROPERTY);
		String decimalSeparatorProperty = properties.getProperty(DECIMAL_SEPARATOR_PROPERTY);
		
		LogUtil.log(MessageFormat.format("Properties specified: playersInputFile:{0}, relevantPlayersPerGroup:{1}, decimalSeparator:{2}.",
				playersInputFileProperty, relevantPlayersPerGroupProperty, decimalSeparatorProperty));
		
		if (playersInputFileProperty == null || playersInputFileProperty.isEmpty()) {
			throw new IllegalArgumentException(PLAYERS_INPUT_FILENAME_PROPERTY + " property must not be empty.");
		}
		else {
			playersInputFile = playersInputFileProperty;
		}
		
		if (relevantPlayersPerGroupProperty == null || relevantPlayersPerGroupProperty.isEmpty()) {
			relevantPlayersPerGroup = Integer.MAX_VALUE;
		}
		else {
			try {
				relevantPlayersPerGroup = Integer.valueOf(relevantPlayersPerGroupProperty);
			}
			catch (NumberFormatException e) {
				throw new IllegalArgumentException(RELEVANT_PLAYERS_PER_GROUP_PROPERTY + " property must be a number.");
			}
		}
		
		if (decimalSeparatorProperty == null || decimalSeparatorProperty.isEmpty()) {
			decimalSeparator = '.';
		}
		else {
			decimalSeparator = decimalSeparatorProperty.equals(",") ? ',' : '.';
		}
		
		LogUtil.log("Loaded properties.");
	}
	
	public static String getPlayersInputFile() {
		return playersInputFile;
	}
		
	public static int getRelevantPlayersPerGroup() {
		return relevantPlayersPerGroup;
	}
	
	public static char getDecimalSeparator() {
		return decimalSeparator;
	}
	
}
