package utility;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogUtil {
		
	private static StringBuilder logfileText = new StringBuilder();
	private static StringWriter sw = new StringWriter();
	private static PrintWriter pw = new PrintWriter(sw);
	
	public static void log(String text, Throwable t) {
		log("Error - " + text);
		t.printStackTrace(pw);
		log(sw.toString());
	}
	
	public static void log(String text) {
		String time = new SimpleDateFormat("HH:mm:ss").format(new Date());
		logfileText.append(time + " " + text + "\n");
	}
	
	public static void appendEmptyLine() {
		logfileText.append("\n");
	}
	
	public static String getLogfileText() {
		return logfileText.toString();
	}
	
	public static void clearLogfileText() {
		logfileText = new StringBuilder();
	}
	
	public static void createLogfile(String path, String filename) {
		
		File file = new File(path);
		file.mkdirs();
		
		file = new File(path + filename);
		FileWriter fw = null;
		try {
			fw = new FileWriter(file);
			fw.write(getLogfileText());
			fw.flush();
		}
		catch (Throwable t) {
			System.out.println("Error - Could not create logfile.");
			t.printStackTrace();
			System.out.println("\nLogfile text:");
			System.out.println(getLogfileText());
		}
		finally {
			if (fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					System.out.println("Error - Could not close FileWriter.");
					e.printStackTrace();
				}
			}
		}
	}
	
}
