package main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import model.Group;
import model.Player;
import utility.LogUtil;
import utility.PropertiesUtil;

public class Main {

	private static final String VERSION = "1.1";
	private static final String PROPERTIES_FILENAME = "config.properties";
	private static final String LOGFILE_OUTPUT_DIRECTORY = "./outputFiles/logs/";
	private static final String REPORTS_OUTPUT_DIRECTORY = "./outputFiles/reports/";
	
	private static final BigDecimal DEFAULT_ELO = new BigDecimal(1000);
		
	public static void main(String[] args) {
		
		Date date = new Date();
		String timeStamp = new SimpleDateFormat("yyyyMMdd-HHmmss").format(date);
		String formattedTimeStamp = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(date);
		String LogfileFilename = "Logfile_" + timeStamp + ".txt";
		String txtReportFilename = "Report_" + timeStamp + ".txt";
		String csvReportFilename = "Report_" + timeStamp + ".csv";
				
		LogUtil.log("Tool started, Version: " + VERSION);
		try {
			try {
				PropertiesUtil.loadProperties(PROPERTIES_FILENAME);
			} catch (IOException e) {
				LogUtil.log("Could not load properties.", e);
				
				return;
			}
			catch (IllegalArgumentException e1) {
				LogUtil.log("Invalid property.", e1);
				
				return;
			}
			
			int relevantPlayersPerGroup = PropertiesUtil.getRelevantPlayersPerGroup();
			
			// Parse groups/players from the input file.
			List<Group> groups = null;
			try {
				groups = FileParser.getGroups(PropertiesUtil.getPlayersInputFile());
			} catch (FileNotFoundException e) {
				LogUtil.log("Could not find input file \"" + PropertiesUtil.getPlayersInputFile() + "\".", e);
				
				return;
			}
			
			// Parse the top list.
			Map<String, BigDecimal> allPlayers = null;
			try {
				allPlayers = HtmlParser.getPlayersElo();
			} catch (IOException e) {
				LogUtil.log("Could not parse LIHL top list.", e);
				
				return;
			}
			
			// add the Elo for the players to parse
			for (Group group : groups) {
				for (Player player : group.getPlayers()) {
					if (allPlayers.containsKey(player.getName())) {
						player.setElo(allPlayers.get(player.getName()));
					}
					else {
						player.setElo(DEFAULT_ELO);
					}
				}
			}
			
			// sort the players and groups by Elo
			groups = sortGroupsAndPlayersByElo(groups, relevantPlayersPerGroup);
			
			// Create the reports.
			LogUtil.log("Creating reports ...");
			
			String txtFilename = FileCreator.createFantasyLeagueReport(REPORTS_OUTPUT_DIRECTORY, txtReportFilename, 
					groups, relevantPlayersPerGroup, formattedTimeStamp, ": ");
			String csvFilename = FileCreator.createFantasyLeagueReport(REPORTS_OUTPUT_DIRECTORY, csvReportFilename, 
					groups, relevantPlayersPerGroup, formattedTimeStamp, ";");
			
			LogUtil.log("Created .txt report: " + txtFilename);
			LogUtil.log("Created .csv report: " + csvFilename);
			
			LogUtil.log("Tool successfully finished. Shutting down.");
		}
		finally {
			LogUtil.createLogfile(LOGFILE_OUTPUT_DIRECTORY, LogfileFilename);
		}
	}
	
	public static List<Group> sortGroupsAndPlayersByElo(List<Group> groups, int relevantPlayersPerGroup) {
		LogUtil.log("Sorting groups and players by Elo ...");
		
		List<Group> groupsSorted = new ArrayList<Group>();
		List<Group> groupsUnsorted = new ArrayList<Group>();
		
		// Sort the players in every group from high Elo to low Elo.
		for (Group group : groups) {
			List<Player> playersSorted = new ArrayList<Player>();
			List<Player> playersUnsorted = group.getPlayers();
			for (Player player : playersUnsorted) {
				if (playersSorted.isEmpty()) {
					playersSorted.add(player);
				}
				else {
					boolean playerAdded = false;
					for (int i = 0; i < playersSorted.size() + 1; i++) {
						if (!playerAdded) {
							if (i == playersSorted.size()) {
								playersSorted.add(player);
								playerAdded = true;
							}
							else {
								if (playersSorted.get(i).getElo().compareTo(player.getElo()) < 0) {
									playersSorted.add(i, player);
									playerAdded = true;;
								}
							}
						}
					}
				}
			}
			groupsUnsorted.add(new Group(group.getName(), playersSorted));
		}

		for (Group group : groupsUnsorted) {
			BigDecimal totalElo = new BigDecimal(0);
			int i = 0;
			for (Player player : group.getPlayers()) {
				if (i >= relevantPlayersPerGroup) {
					break;
				}
				totalElo = totalElo.add(player.getElo());
				i++;
			}
			BigDecimal avgElo = i == 0 ? new BigDecimal(0) : totalElo.divide(new BigDecimal(i), 2, RoundingMode.HALF_UP);
			group.setAvgElo(avgElo);
		}
		
		// Sort the groups by their average Elo.
		for (Group group : groupsUnsorted) {
			if (groupsSorted.isEmpty()) {
				groupsSorted.add(group);
			}
			else {
				boolean groupAdded = false;
				for (int i = 0; i < groupsSorted.size() + 1; i++) {
					if (!groupAdded) {
						if (i == groupsSorted.size()) {
							groupsSorted.add(group);
							groupAdded = true;
						}
						else {
							if (groupsSorted.get(i).getAvgElo().compareTo(group.getAvgElo()) < 0) {
								groupsSorted.add(i, group);
								groupAdded = true;;
							}
						}
					}
				}
			}
		}
		
		LogUtil.log("Sorted groups and players by Elo.");
		
		return groupsSorted;
	}
	
	public static String getOutputDirectory() {
		return LOGFILE_OUTPUT_DIRECTORY;
	}
}
