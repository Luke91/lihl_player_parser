package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.util.List;

import utility.LogUtil;
import utility.PropertiesUtil;
import model.Group;
import model.Player;

public class FileCreator {
		
	public static String createFantasyLeagueReport(String path, String filename, List<Group> groups, int relevantPlayersPerGroup,
			String timeStamp, String seperator) {
		StringBuilder content = new StringBuilder();
		
		content.append("LIHL Fantasy League" + seperator + timeStamp + "\n\n");
		content.append("Average Elo");
		if (relevantPlayersPerGroup != Integer.MAX_VALUE) {
			 content.append(" of highest " + relevantPlayersPerGroup + " players per group");
		}
		content.append(":\n");
		for (Group group: groups) {
			content.append(group.getName() + seperator + replaceDecimalSeparator(group.getAvgElo().toPlainString()) + "\n");
		}
		
		for (Group group : groups) {
			content.append("\n" + group.getName() + ":\n");
			for (Player player : group.getPlayers()) {
				String elo = replaceDecimalSeparator(player.getElo().setScale(2, RoundingMode.HALF_UP).toPlainString());
				content.append(player.getName() + seperator + elo + "\n");
			}
		}
		
		File file = new File(path);
		file.mkdirs();
		
		file = new File(path + filename);
		FileWriter fw = null;
		try {
			fw = new FileWriter(file);
			fw.write(content.toString());
			fw.flush();
			
			return file.getAbsolutePath();
		}
		catch (Throwable t) {
			LogUtil.log("Could not create file: " + file.getAbsolutePath(), t);
			
			return null;
		}
		finally {
			if (fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					LogUtil.log("Could not close FileWriter.", e);
				}
			}
		}
	}
	
	private static String replaceDecimalSeparator(String s) {
		String ret = "";
		if (s.contains(".")) {
			ret = s.replace('.', PropertiesUtil.getDecimalSeparator());
		}
		else if (s.contains(",")) {
			ret = s.replace(',', PropertiesUtil.getDecimalSeparator());
		}
		
		return ret;
	}
}
