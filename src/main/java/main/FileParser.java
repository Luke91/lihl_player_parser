package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import utility.LogUtil;
import model.Group;
import model.Player;

public class FileParser {
	
	public static List<Group> getGroups(String filename) throws FileNotFoundException {
		List<Group> groups = new ArrayList<Group>();
		File file = new File(filename);
		
		LogUtil.log("Parsing groups from: " + file.getAbsolutePath());
		
		Scanner scanner = new Scanner(file);
		Group group = null;
		boolean newGroup = true;
		
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			
			if (newGroup) {
				group = new Group(line);
				newGroup = false;
			}
			else if (line.isEmpty()) {
				newGroup = true;
				
				if (group != null) {
					groups.add(group);
				}
				group = null;
			}
			else {
				Player player = new Player(line.toLowerCase());
				group.addPlayer(player);
			}
		}
		
		if (group != null) {
			groups.add(group);
		}
		
		scanner.close();
		
		LogUtil.log("Successfully parsed " + groups.size() + " groups.");
		
		return groups;
	}

}
