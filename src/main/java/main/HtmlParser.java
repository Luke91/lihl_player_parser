package main;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import utility.LogUtil;

public class HtmlParser {

	private static final String LIHL_LINK_START = "https://entgaming.net/customstats/lihl/top/";
	private static final String LIHL_LINK_END = "/";
	private static final String LIHL_PLAYER_SELECTOR = "html body center div.container div.content table.zebra-striped tbody tr.TableRow28";

	public static Map<String, BigDecimal> getPlayersElo() throws IOException {
		LogUtil.log("Parsing LIHL top players pages ...");
		
		Map<String, BigDecimal> players = new HashMap<String, BigDecimal>();
		
		Elements elements = new Elements();
		boolean emptyPage = false;
		int page = 1;
		
		// get all player elements from the top list
		while (!emptyPage) {
			LogUtil.log("Parsing page " + page + "...");
			Document doc = Jsoup.connect(LIHL_LINK_START + page + LIHL_LINK_END).userAgent("Mozilla").get();
			Elements currentPage = doc.select(LIHL_PLAYER_SELECTOR);
			LogUtil.log("Parsed page " + page + ".");
			if (currentPage.size() > 0) {
				elements.addAll(currentPage);
				page++;
			}
			else {
				emptyPage = true;
			}
		}
		
		// get the player name and Elo of every element
		Iterator<Element> iter = elements.iterator();
		
		while (iter.hasNext()) {
			Element elem = iter.next();
			String player = elem.child(0).text();
			BigDecimal elo = new BigDecimal(elem.child(1).text());
			
			players.put(player, elo);
		}
		
		LogUtil.log("Successfully Parsed LIHL top players pages.");
		
		return players;
	}	
	
}
