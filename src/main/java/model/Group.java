package model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Group {
	
	private String name;
	
	private List<Player> players;
	
	private BigDecimal avgElo;
	
	public Group() {
		
	}
	
	public Group(String name) {
		this.name = name;
	}
	
	public Group(String name, List<Player> players) {
		this.name = name;
		this.players = players;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	
	public void addPlayer(Player player) {
		if (players == null) {
			players = new ArrayList<Player>();
		}
		players.add(player);
	}
	
	public BigDecimal getAvgElo() {
		return avgElo;
	}
	
	public void setAvgElo(BigDecimal avgElo) {
		this.avgElo = avgElo == null ? new BigDecimal(0) : avgElo;
	}
	
}
