package model;

import java.math.BigDecimal;

public class Player {
	
	private String name;
	
	private BigDecimal elo;
	
	public Player() {
		elo = new BigDecimal(0);
	}
	
	public Player(String name) {
		this.name = name;
	}
	
	public Player(String name, BigDecimal elo) {
		this.name = name;
		this.elo = elo;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
		
	public BigDecimal getElo() {
		return elo;
	}

	public void setElo(BigDecimal elo) {
		this.elo = elo == null ? new BigDecimal(0) : elo;
	}
}
