# README #

### What is this repository for? ###

LIHL player parser automatically creates reports displaying Elo of the Legion TD Inhouse League players specified in an input file. In that input file players can be split to different groups and for every group also the average Elo of the highest x players - with x being specified in a properties file - is being calculated.

Current Version: 1.1

Java Version 1.5 or higher is required.

### How to build?###

* Check the repository at https://bitbucket.org/Luke91/lihl_player_parser/overview out.
* Build the software with maven, e.g. run "mvn install".
* Two .jar files are created, one with and one without dependencies.

### How to configure?###

Create a "config.properties" file with following properties (also see "config.properties" in the repo):

* playersInputFile: A relative or absolute path to the input file containing the groups and players to parse. Must not be empty.
* relevantPlayersPerGroup: The amount of (highest Elo) players per group that should be considered for calculating the average Elo per group. If empty or not existent it considers all players per group (up to Integer.MAX_VALUE).
* decimalSeparator: The decimal separator used in the created reports (e.g. useful for the .csv report so that the opening tool correctly interprets the values). If empty, not existent or invalid value a dot (".") is being used.

### The input file###

The path to the input file must be specified in the file "config.properties".  
  
The format of it must be (also see "players.txt" in the repo):  
Group1 name  
player1 of group1  
player2 of group1  
player3 of group1  

Group2 name  
player1 of group2  
player2 of group2  
player3 of group2  
...  

Group names can be of any format.  
Player names must be equivalent to the names displayed on the LIHL top players page - https://entgaming.net/customstats/lihl/top/ but are case insensitive.  

### How to use?###

* Build the software as mentioned above.
* Create a "config.properties" file as mentioned above.
* Create an input file as mentioned above.
* Run the created .jar file (e.g. double click).
* Check the created logfile in ./outputFiles/logs/
* Check the created reports in ./outputFiles/reports/